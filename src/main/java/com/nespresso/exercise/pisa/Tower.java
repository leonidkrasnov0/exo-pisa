package com.nespresso.exercise.pisa;

public class Tower {

    private Floor firstFloor;
    private Floor topFloor;
    private int size;

    public Tower() {
        firstFloor = null;
        topFloor = null;
        size = 0;
    }

    public void addFloor(final int floorSize) {
        addFloor(new Floor(floorSize, 0));
    }

    public String printFloor(final int floorIndex) {
        Floor floor = getFloor(floorIndex);
        String floorStr = "";

        int windowsRemaining = floor.getWindows();
        int maxWindowGroup = getMaxWindowsGroup(floor);
        int windowStartingPos = (floor.getSize() - floor.getWindows()) / 2;
        int currentWindowGroup = 0;

        for(int i = 0; i < floor.getSize(); i++) {
            if (i < windowStartingPos || windowsRemaining == 0 || currentWindowGroup == maxWindowGroup) {
                floorStr = floorStr + "X";
                currentWindowGroup = 0;
                continue;
            }

            floorStr = floorStr + "0";
            windowsRemaining--;
            currentWindowGroup++;
        }


        return floorStr;
    }

    public void addFloorWithWindows(final int floorSize, final int desiredNumberOfWindows) {
        int numberOfWindows = desiredNumberOfWindows >= floorSize - 1 ? floorSize - 2 : desiredNumberOfWindows;
        addFloor(new Floor(floorSize, numberOfWindows));
    }

    private void addFloor(final Floor floor) {
        if(firstFloor == null) {
            firstFloor = floor;
            topFloor = firstFloor;
        }
        else {
            if(topFloor.getSize() < floor.getSize()) {
                throw new IllegalArgumentException("You cannot add a floor larger than the lower floor");
            }

            topFloor.setNextFloor(floor);
            topFloor = floor;
        }

        size++;
    }

    private int numOfFloorsAbove(final Floor floor) {
        if (floor.getNextFloor() == null) {
            return 0;
        }

        return 1 + numOfFloorsAbove(floor.getNextFloor());
    }

    private Floor getFloor(final int floorIndex) {
        if(floorIndex < 0) {
            throw new IllegalArgumentException("There are no basement floors");
        }
        else if(floorIndex > size) {
            throw new IllegalArgumentException("You asked for floor " + floorIndex + ", There are only " + size + " floors in this Tower.");
        }

        int ctr = 0;
        Floor floor = firstFloor;
        while (ctr < floorIndex) {
            floor = floor.getNextFloor();
            ctr++;
        }

        return floor;
    }

    private int getMaxWindowsGroup(final Floor floor) {
        int windowGroup;

        switch (numOfFloorsAbove(floor)) {
            case 0:
                windowGroup = floor.getWindows();
                break;
            case 1:
                windowGroup = 3;
                break;
            case 2:
                windowGroup = 2;
                break;
            default:
                windowGroup = 1;
        }

        return windowGroup;
    }

    private class Floor {
        private Floor nextFloor;
        private int size;
        private int windows;

        Floor(final int size, final int windows) {
            this.size = size;
            this.windows = windows;
        }

        public int getSize() {
            return size;
        }

        public int getWindows() {
            return windows;
        }

        public Floor getNextFloor() {
            return nextFloor;
        }

        public void setNextFloor(final Floor nextFloor) {
            this.nextFloor = nextFloor;
        }

    }

}
